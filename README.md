This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project belongs to a serie of test application I'll create to study a bit more about React in general.

The content of the trainning are available at: https://www.udemy.com/react-redux/learn/v4/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).