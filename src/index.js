//Import React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';

//Create a react component
const App = () => {
    let text = 'I`m here, can you see me?';
    return <div>
        <h1 style={{border : '1px solid red', color : 'green'}}>Hello World</h1>
        <p>{text }</p>
        <label htmlFor="input">Here we go with the input</label>
        <input id="input"/>
        </div>;
};

//Take the react component and show it to the screen
ReactDOM.render(
    <App />,
    document.querySelector('#root')
);